import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    double len = MediaQuery.of(context).size.height / 1.5;
    double len1 = MediaQuery.of(context).size.height / 5;
    double len2 = MediaQuery.of(context).size.height / 20;
    double width = MediaQuery.of(context).size.width * 0.8;

    _profileBoxes(IconData icon, String name, String quantity, String desc) {
      return Padding(
        padding: const EdgeInsets.all(10.0),
        child: Container(
          height: MediaQuery.of(context).size.height * 0.13,
          width: MediaQuery.of(context).size.width * 0.4,
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.grey,
                    offset: Offset(10, 10),
                    spreadRadius: 0,
                    blurRadius: 5)
              ],
              borderRadius: BorderRadius.circular(20)),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                        height: 25,
                        width: 25,
                        decoration: BoxDecoration(
                            color: Colors.red[100],
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            )),
                        child: Icon(
                          icon,
                          size: 20,
                          color: Colors.red[900],
                        )),
                    Padding(
                      padding: const EdgeInsets.only(left:10.0),
                      child: Text(
                        name,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.grey,
                            fontSize: 12.0),
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Row(
                    children: <Widget>[
                      Text(
                        quantity,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 18.0),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: Text(
                          desc,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Colors.grey,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }

    Widget portrait() {
      return Container(
          child: Stack(children: <Widget>[
        Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                    bottomRight: Radius.circular(30),
                  ),
                  child: Container(
                    height: 300.0,
                    width: double.infinity,
                    color: Colors.red[900],
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0, top: 60),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("My",
                              style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  color: Colors.white,
                                  fontSize: 20)),
                          Text("Profile",
                              style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white,
                                  fontSize: 20)),
                        ],
                      ),
                    ),
                  ),
                ),

// heading icons
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(
                          Icons.arrow_back,
                          color: Colors.white,
                        ),
                        Icon(
                          Icons.notifications,
                          color: Colors.white,
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),

// this is the white container just under the red one
        Positioned(
          child: Align(
            alignment: FractionalOffset.bottomCenter,
            child: Container(
              height: len,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30)),
                color: Colors.white,
              ),
              child: Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        _profileBoxes(
                            Icons.done_all, "Livraisons", "34", "effectués"),
                        _profileBoxes(
                            Icons.crop_din, "Commandes", "34", "a faire")
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        _profileBoxes(Icons.shop, "Offres", "3", "jdsj"),
                        _profileBoxes(
                            Icons.directions_run, "en cour", "4", "loading")
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        _profileBoxes(
                            Icons.sentiment_satisfied, "Note", "5", "*****"),
                        _profileBoxes(Icons.show_chart, "Gains", "3400", "\$")
                      ],
                    ),
                    SizedBox(
                      height: len2,
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        //this is the container in which we have the picture, the name.....
        SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 130.0),
            child: Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: len1,
                width: width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey[300],
                          offset: Offset(0, 10),
                          spreadRadius: 0,
                          blurRadius: 5)
                    ]),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 55.0),
                      child: Text(
                        "Arsene Kelly",
                        style: TextStyle(fontWeight: FontWeight.w700),
                      ),
                    ),
                    Text(
                      "PID:23456AHDE",
                      style: TextStyle(fontWeight: FontWeight.w300),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
// this is the image
        Padding(
          padding: const EdgeInsets.only(top: 100.0),
          child: Align(
            alignment: Alignment.topCenter,
            child: Container(
              width: 80.0,
              height: 80.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage("images/cuteguy.jpg"),
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
        ),
      ]));
    }

    Widget landscape() {
      return Container(color: Colors.green);
    }

    return Scaffold(
        backgroundColor: Colors.red[900],
        body: OrientationBuilder(builder: (context, orientation) {
          if (orientation == Orientation.portrait) {
            return portrait();
          } else {
            return landscape();
          }
        }));
  }
}
