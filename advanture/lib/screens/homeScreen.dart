import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var controller;

  // ignore: must_call_super
  void initState() {
    controller = new PageController(viewportFraction: 0.8);
  }

  @override
  Widget build(BuildContext context) {
    double len = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    Widget _customer(
      String text2,
      String text3,
      String text4,
      String image,
    ) {
      return Container(
          child: Padding(
        padding: EdgeInsets.all(8),
        child: Container(
          color: Colors.greenAccent,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(height: MediaQuery.of(context).size.height * 0.01),
              Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(image: AssetImage(image))),
                child: Stack(
                  children: [
                    Positioned(
                      bottom: 0,
                      right: 0,
                      child: Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.yellow,
                        ),
                        child: Icon(Icons.done_all, color: Colors.white),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.03),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  text2,
                  style: TextStyle(
                    decoration: TextDecoration.none,
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      text3,
                      overflow: TextOverflow.fade,
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                        decoration: TextDecoration.none,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      text4,
                      textAlign: TextAlign.start,
                      overflow: TextOverflow.fade,
                      style: TextStyle(
                        fontSize: 13,
                        color: Colors.white,
                        decoration: TextDecoration.none,
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ));
    }

    Widget _card2(
      String text2,
      String text3,
    ) {
      return Expanded(
          child: Padding(
        padding: EdgeInsets.all(8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Card(
                  elevation: 0,
                  color: Colors.transparent,
                  child: Text(
                    text2,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: MediaQuery.of(context).size.height * 0.085,
                    ),
                  )),
            ),
            Center(
              child: Card(
                elevation: 0,
                color: Colors.transparent,
                child: Text(
                  text3,
                  overflow: TextOverflow.fade,
                  style: TextStyle(
                      fontSize: MediaQuery.of(context).size.height * 0.03,
                      color: Colors.white),
                ),
              ),
            )
          ],
        ),
      ));
    }

    _mostPopularRestaurants(AssetImage img, String destination, String rating,
        String description, String dest, String buttonName) {
      return Container(
        width: width,
        child: Column(
          children: <Widget>[
            Container(
                height: len / 2.3,
                width: width * 0.95,
                decoration: BoxDecoration(
                    image: DecorationImage(image: img, fit: BoxFit.cover))),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    destination,
                    style: TextStyle(
                        decoration: TextDecoration.none,
                        color: Colors.black,
                        fontSize: MediaQuery.of(context).size.height * 0.03),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(6.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SmoothStarRating(
                        allowHalfRating: false,
                        onRated: (v) {},
                        starCount: 5,
                        rating: 4,
                        size: 20.0,
                        isReadOnly: true,
                        filledIconData: Icons.star,
                        halfFilledIconData: Icons.star_half,
                        defaultIconData: Icons.star_border,
                        color: Colors.yellow[300],
                        borderColor: Colors.yellow[300],
                        spacing: 0.0),
                    Text(
                      rating,
                      style: TextStyle(
                          decoration: TextDecoration.none,
                          color: Colors.black,
                          fontSize: MediaQuery.of(context).size.height * 0.02,
                          fontWeight: FontWeight.normal),
                    )
                  ]),
            ),
            Padding(
              padding: EdgeInsets.all(5),
              child: Container(
                width: width,
                child: Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Text(
                          description,
                          style: TextStyle(
                              decoration: TextDecoration.none,
                              color: Colors.black,
                              fontSize:
                                  MediaQuery.of(context).size.height * 0.025,
                              fontWeight: FontWeight.normal),
                        ),
                      ),
                      SizedBox(
                        height: len / 20,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                  child: Divider(
                color: Colors.grey,
              )),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(Icons.map),
                        Text(
                          dest,
                          style: TextStyle(
                              decoration: TextDecoration.none,
                              color: Colors.black,
                              fontSize:
                                  MediaQuery.of(context).size.height * 0.02,
                              fontWeight: FontWeight.normal),
                        )
                      ]),
                  GestureDetector(
                    child: Container(
                      width: width * 0.3,
                      height: len / 15,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.green),
                      child: Center(
                          child: Text(
                        buttonName,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            decoration: TextDecoration.none,
                            color: Colors.white,
                            fontSize: MediaQuery.of(context).size.height * 0.03,
                            fontWeight: FontWeight.normal),
                      )),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: len / 20)
          ],
        ),
      );
    }

    _mostPopularDestinations(
        AssetImage img,
        String destination,
        String price,
        String rating,
        String description,
        String duration,
        String dest,
        String buttonName) {
      return Container(
        width: width,
        child: Column(
          children: <Widget>[
            Container(
                height: len / 2.3,
                width: width * 0.95,
                decoration: BoxDecoration(
                    image: DecorationImage(image: img, fit: BoxFit.cover))),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    destination,
                    style: TextStyle(
                        decoration: TextDecoration.none,
                        color: Colors.black,
                        fontSize: MediaQuery.of(context).size.height * 0.03),
                  ),
                  Text(
                    price,
                    style: TextStyle(
                        decoration: TextDecoration.none,
                        color: Colors.blue,
                        fontSize: MediaQuery.of(context).size.height * 0.03),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(6.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SmoothStarRating(
                        allowHalfRating: false,
                        onRated: (v) {},
                        starCount: 5,
                        rating: 4,
                        size: 20.0,
                        isReadOnly: true,
                        filledIconData: Icons.star,
                        halfFilledIconData: Icons.star_half,
                        defaultIconData: Icons.star_border,
                        color: Colors.yellow[300],
                        borderColor: Colors.yellow[300],
                        spacing: 0.0),
                    Text(
                      rating,
                      style: TextStyle(
                          decoration: TextDecoration.none,
                          color: Colors.black,
                          fontSize: MediaQuery.of(context).size.height * 0.02,
                          fontWeight: FontWeight.normal),
                    )
                  ]),
            ),
            Padding(
              padding: EdgeInsets.all(5),
              child: Container(
                width: width,
                child: Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Text(
                          description,
                          style: TextStyle(
                              decoration: TextDecoration.none,
                              color: Colors.black,
                              fontSize:
                                  MediaQuery.of(context).size.height * 0.025,
                              fontWeight: FontWeight.normal),
                        ),
                      ),
                      SizedBox(
                        height: len / 20,
                      ),
                      Text(
                        duration,
                        style: TextStyle(
                            decoration: TextDecoration.none,
                            color: Colors.black,
                            fontSize: MediaQuery.of(context).size.height * 0.02,
                            fontWeight: FontWeight.normal),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                  child: Divider(
                color: Colors.grey,
              )),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(Icons.map),
                        Text(
                          dest,
                          style: TextStyle(
                              decoration: TextDecoration.none,
                              color: Colors.black,
                              fontSize:
                                  MediaQuery.of(context).size.height * 0.02,
                              fontWeight: FontWeight.normal),
                        )
                      ]),
                  GestureDetector(
                    child: Container(
                      width: width * 0.3,
                      height: len / 15,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.green),
                      child: Center(
                          child: Text(
                        buttonName,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            decoration: TextDecoration.none,
                            color: Colors.white,
                            fontSize: MediaQuery.of(context).size.height * 0.02,
                            fontWeight: FontWeight.normal),
                      )),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: len / 20)
          ],
        ),
      );
    }

    _mostPopularHotels(
        AssetImage img,
        String destination,
        String price,
        String rating,
        String description,
        String duration,
        String dest,
        String buttonName) {
      return Container(
        width: width,
        child: Column(
          children: <Widget>[
            Container(
                height: len / 2.3,
                width: width * 0.95,
                decoration: BoxDecoration(
                    image: DecorationImage(image: img, fit: BoxFit.cover))),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    destination,
                    style: TextStyle(
                        decoration: TextDecoration.none,
                        color: Colors.black,
                        fontSize: MediaQuery.of(context).size.height * 0.03),
                  ),
                  Text(
                    price,
                    style: TextStyle(
                        decoration: TextDecoration.none,
                        color: Colors.blue,
                        fontSize: MediaQuery.of(context).size.height * 0.03),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(6.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SmoothStarRating(
                            allowHalfRating: false,
                            onRated: (v) {},
                            starCount: 5,
                            rating: 4,
                            size: 20.0,
                            isReadOnly: true,
                            filledIconData: Icons.star,
                            halfFilledIconData: Icons.star_half,
                            defaultIconData: Icons.star_border,
                            color: Colors.yellow[300],
                            borderColor: Colors.yellow[300],
                            spacing: 0.0),
                        Text(
                          rating,
                          style: TextStyle(
                              decoration: TextDecoration.none,
                              color: Colors.black,
                              fontSize:
                                  MediaQuery.of(context).size.height * 0.02,
                              fontWeight: FontWeight.normal),
                        )
                      ]),
                  Text(
                    duration,
                    style: TextStyle(
                        decoration: TextDecoration.none,
                        color: Colors.blue,
                        fontSize: MediaQuery.of(context).size.height * 0.03),
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(5),
              child: Container(
                width: width,
                child: Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Text(
                          description,
                          style: TextStyle(
                              decoration: TextDecoration.none,
                              color: Colors.black,
                              fontSize:
                                  MediaQuery.of(context).size.height * 0.025,
                              fontWeight: FontWeight.normal),
                        ),
                      ),
                      SizedBox(height: len / 40)
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                  child: Divider(
                color: Colors.grey,
              )),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(Icons.map),
                        Text(
                          dest,
                          style: TextStyle(
                              decoration: TextDecoration.none,
                              color: Colors.black,
                              fontSize:
                                  MediaQuery.of(context).size.height * 0.02,
                              fontWeight: FontWeight.normal),
                        )
                      ]),
                  GestureDetector(
                    child: Container(
                      width: width * 0.3,
                      height: len / 15,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.green),
                      child: Center(
                          child: Text(
                        buttonName,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            decoration: TextDecoration.none,
                            color: Colors.white,
                            fontSize: MediaQuery.of(context).size.height * 0.03,
                            fontWeight: FontWeight.normal),
                      )),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: len / 20)
          ],
        ),
      );
    }

    _flyer(IconData icon, String title, String desc) {
      return Padding(
        padding: const EdgeInsets.all(10.0),
        child: Container(
          width: width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Icon(icon, size: 60, color: Colors.yellowAccent),
              SizedBox(
                height: len / 35,
              ),
              Text(
                title,
                style: TextStyle(
                    decoration: TextDecoration.none,
                    color: Colors.black,
                    fontSize: MediaQuery.of(context).size.height * 0.04),
              ),
              SizedBox(
                height: len / 40,
              ),
              Text(
                desc,
                style: TextStyle(
                    decoration: TextDecoration.none,
                    color: Colors.black,
                    fontSize: MediaQuery.of(context).size.height * 0.018,
                    fontWeight: FontWeight.normal),
              )
            ],
          ),
        ),
      );
    }

    _imageplustext(AssetImage img, String title, String text) {
      return Container(
        child: Column(
          children: <Widget>[
            Container(
                height: len / 3,
                width: width,
                decoration: BoxDecoration(
                    image: DecorationImage(image: img, fit: BoxFit.cover))),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                  child: Text(
                title,
                style: TextStyle(
                    decoration: TextDecoration.none,
                    color: Colors.black,
                    fontSize: MediaQuery.of(context).size.height * 0.05),
              )),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                  child: Text(
                text,
                style: TextStyle(
                    decoration: TextDecoration.none,
                    color: Colors.black,
                    fontSize: MediaQuery.of(context).size.height * 0.02,
                    fontWeight: FontWeight.normal),
              )),
            )
          ],
        ),
      );
    }

    _imagecards(AssetImage img, String name) {
      return Padding(
        padding: const EdgeInsets.all(10.0),
        child: Container(
            height: len / 2.3,
            width: width * 0.9,
            decoration: BoxDecoration(
                image: DecorationImage(image: img, fit: BoxFit.cover)),
            child: Stack(
              children: <Widget>[
                Positioned(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        width: width * 0.7,
                        height: len / 10,
                        decoration: BoxDecoration(color: Colors.white),
                        child: Center(
                          child: Text(
                            name,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              decoration: TextDecoration.none,
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize:
                                  MediaQuery.of(context).size.height * 0.04,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )),
      );
    }

    _textDescription(String numb, String name, String desc) {
      return Padding(
        padding: EdgeInsets.all(20),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      numb,
                      style: TextStyle(
                          color: Colors.yellow[400],
                          decoration: TextDecoration.none,
                          fontWeight: FontWeight.normal,
                          fontSize: MediaQuery.of(context).size.height * 0.05),
                    ),
                    SizedBox(width: width / 80),
                    Text(
                      name,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.none,
                          color: Colors.black,
                          fontSize: MediaQuery.of(context).size.height * 0.05),
                    )
                  ],
                ),
              ),
              Text(desc,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.none,
                      color: Colors.black38,
                      fontSize: MediaQuery.of(context).size.height * 0.02))
            ],
          ),
        ),
      );
    }

    _nameplustextfield(String name, IconData icon) {
      return Padding(
        padding: const EdgeInsets.all(6.0),
        child: Container(
          width: width * 0.8,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(name),
              TextField(
                decoration: InputDecoration(
                    hintText: name,
                    icon: new Icon(
                      icon,
                      color: Colors.yellow[300],
                    )),
              )
            ],
          ),
        ),
      );
    }

    _button() {
      return GestureDetector(
        child: Container(
          width: width * 0.8,
          height: len / 15,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.yellow[300]),
          child: Center(
              child: Text(
            "Search",
            style:
                TextStyle(fontSize: MediaQuery.of(context).size.height * 0.05),
          )),
        ),
      );
    }

    _flight() {
      return SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _nameplustextfield("From", Icons.flight),
            _nameplustextfield("Where", Icons.gps_not_fixed),
            _nameplustextfield("Check in", Icons.location_on),
            _nameplustextfield("Check out", Icons.location_on),
            _nameplustextfield("Travellers", Icons.person),
            _button()
          ],
        ),
      );
    }

    _car() {
      return SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _nameplustextfield("Where", Icons.gps_not_fixed),
            _nameplustextfield("Check in", Icons.location_on),
            _nameplustextfield("Check out", Icons.location_on),
            SizedBox(
              height: len / 4,
            ),
            _button()
          ],
        ),
      );
    }

    _hotel() {
      return SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _nameplustextfield("Check in", Icons.location_on),
            _nameplustextfield("Check out", Icons.location_on),
            _nameplustextfield("Guest", Icons.arrow_drop_down),
            SizedBox(
              height: len / 4,
            ),
            _button()
          ],
        ),
      );
    }

    _imagecardsplustext(AssetImage img, String name, String desc,
        String message, String longdesc) {
      return Padding(
        padding: const EdgeInsets.all(10.0),
        child: Container(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: len / 30,
              ),
              Container(
                  height: len / 2.3,
                  width: width * 0.9,
                  decoration: BoxDecoration(
                      image: DecorationImage(image: img, fit: BoxFit.cover)),
                  child: Stack(
                    children: <Widget>[
                      Positioned(
                        child: Align(
                          alignment: Alignment.bottomLeft,
                          child: Padding(
                            padding: const EdgeInsets.all(0.0),
                            child: Container(
                              width: width * 0.3,
                              height: len / 15,
                              decoration: BoxDecoration(color: Colors.green),
                              child: Center(
                                child: Text(
                                  name,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    decoration: TextDecoration.none,
                                    color: Colors.white,
                                    fontWeight: FontWeight.normal,
                                    fontSize:
                                        MediaQuery.of(context).size.height *
                                            0.03,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),
              SizedBox(
                height: len / 40,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  longdesc,
                  style: TextStyle(
                    decoration: TextDecoration.none,
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: MediaQuery.of(context).size.height * 0.03,
                  ),
                ),
              ),
              SizedBox(
                height: len / 40,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      desc,
                      style: TextStyle(
                        decoration: TextDecoration.none,
                        color: Colors.grey,
                        fontWeight: FontWeight.normal,
                        fontSize: MediaQuery.of(context).size.height * 0.027,
                      ),
                    ),
                    Icon(
                      Icons.message,
                      color: Colors.grey,
                    ),
                    Text(
                      message,
                      style: TextStyle(
                        decoration: TextDecoration.none,
                        color: Colors.grey,
                        fontWeight: FontWeight.normal,
                        fontSize: MediaQuery.of(context).size.height * 0.027,
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: len / 30,
              ),
            ],
          ),
        ),
      );
    }

    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Container(
              width: width,
              height: len,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('images/bg_1.jpg'), fit: BoxFit.cover)),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 3,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Discover",
                          style: TextStyle(
                              decoration: TextDecoration.none,
                              color: Colors.white,
                              fontSize:
                                  MediaQuery.of(context).size.height * 0.05),
                        ),
                        Text(
                          "A New Place",
                          style: TextStyle(
                              decoration: TextDecoration.none,
                              color: Colors.white,
                              fontSize:
                                  MediaQuery.of(context).size.height * 0.05),
                        ),
                        Text(
                          "Find great places to stay, eat, shop, or visit from local experts",
                          style: TextStyle(
                              decoration: TextDecoration.none,
                              color: Colors.white,
                              fontSize:
                                  MediaQuery.of(context).size.height * 0.018,
                              fontWeight: FontWeight.normal),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            DefaultTabController(
              length: 3,
              child: Container(
                height: len,
                width: width,
                child: Scaffold(
                  appBar: AppBar(
                    bottom: TabBar(
                      tabs: [
                        Tab(
                          icon: Icon(Icons.flight),
                          text: 'Flight',
                        ),
                        Tab(
                          icon: Icon(Icons.hotel),
                          text: 'Hotel',
                        ),
                        Tab(
                          icon: Icon(Icons.directions_car),
                          text: 'Car Rent',
                        ),
                      ],
                    ),
                  ),
                  body: TabBarView(children: [
                    _flight(),
                    _hotel(),
                    _car(),
                  ]),
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(color: Colors.blue[50]),
              child: Column(
                children: <Widget>[
                  _textDescription("01", "Travel",
                      "A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth."),
                  _textDescription("02", "Experience",
                      "A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth."),
                  _textDescription("03", "Relax",
                      "A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.")
                ],
              ),
            ),
            Container(
              height: len / 2,
              decoration: BoxDecoration(color: Colors.white),
              child: Center(
                  child: Text(
                "See our latest vacation ideas",
                textAlign: TextAlign.center,
                style: TextStyle(
                  decoration: TextDecoration.none,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: MediaQuery.of(context).size.height * 0.05,
                ),
              )),
            ),
            _imagecards(
                AssetImage('images/destination-1.jpg'), "BeachFront Scape"),
            _imagecards(
                AssetImage('images/destination-2-1.jpg'), "Group Holidays"),
            _imagecards(AssetImage('images/destination-3.jpg'), "City Breaks"),
            _imageplustext(
                AssetImage('images/about.jpg'),
                "The Best Travel Agency",
                "On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word  and the Little Blind Text should turn around and return to its own, safe country. But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their."),
            Container(
              decoration: BoxDecoration(color: Colors.blue[50]),
              child: Column(
                children: <Widget>[
                  SizedBox(height: len / 10),
                  _flyer(Icons.directions_boat, "Special Activities",
                      "A small river named Duden flows by their place and supplies."),
                  _flyer(Icons.mobile_screen_share, "Travel Arrangements",
                      "A small river named Duden flows by their place and supplies."),
                  _flyer(Icons.contact_mail, "Travel Arrangements",
                      "A small river named Duden flows by their place and supplies."),
                  _flyer(Icons.map, "Location Manager",
                      "A small river named Duden flows by their place and supplies."),
                  SizedBox(height: len / 10),
                ],
              ),
            ),
            Container(
              height: len / 2,
              decoration: BoxDecoration(color: Colors.white),
              child: Center(
                  child: Text(
                "Most Popular Destinations",
                textAlign: TextAlign.center,
                style: TextStyle(
                  decoration: TextDecoration.none,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: MediaQuery.of(context).size.height * 0.05,
                ),
              )),
            ),
            _mostPopularDestinations(
              AssetImage('images/destination-1.jpg'),
              "Paris, Italy",
              "\$200",
              "8 Ratings",
              "Far far away, behind the word mountains, far from the countries",
              "2 days 3 nights",
              "San Francisco, CA",
              "Discover",
            ),
            _mostPopularDestinations(
              AssetImage('images/destination-2.jpg'),
              "Paris, Italy",
              "\$200",
              "8 Ratings",
              "Far far away, behind the word mountains, far from the countries",
              "2 days 3 nights",
              "San Francisco, CA",
              "Discover",
            ),
            _mostPopularDestinations(
              AssetImage('images/destination-3.jpg'),
              "Paris, Italy",
              "\$200",
              "8 Ratings",
              "Far far away, behind the word mountains, far from the countries",
              "2 days 3 nights",
              "San Francisco, CA",
              "Discover",
            ),
            _mostPopularDestinations(
              AssetImage('images/destination-4.jpg'),
              "Paris, Italy",
              "\$200",
              "8 Ratings",
              "Far far away, behind the word mountains, far from the countries",
              "2 days 3 nights",
              "San Francisco, CA",
              "Discover",
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                  image: DecorationImage(
                image: AssetImage("images/bg_1.jpg"),
                fit: BoxFit.fill,
              )),
              child: Stack(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    color: Colors.green[300].withOpacity(0.9),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        _card2("100,000", "Happy Customers"),
                        _card2("40,000", "Destination Places"),
                        _card2("87,000", "Hotels"),
                        _card2("56,400", "Restaurant"),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: len / 2,
              decoration: BoxDecoration(color: Colors.white),
              child: Center(
                  child: Text(
                "Popular Hotels",
                textAlign: TextAlign.center,
                style: TextStyle(
                  decoration: TextDecoration.none,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: MediaQuery.of(context).size.height * 0.05,
                ),
              )),
            ),
            _mostPopularHotels(
              AssetImage('images/hotel-1.jpg'),
              "Paris, Italy",
              "\$200",
              "8 Ratings",
              "Far far away, behind the word mountains, far from the countries",
              "/ night",
              "San Francisco, CA",
              "Book now",
            ),
            _mostPopularHotels(
              AssetImage('images/hotel-2.jpg'),
              "Paris, Italy",
              "\$200",
              "8 Ratings",
              "Far far away, behind the word mountains, far from the countries",
              "/ night",
              "San Francisco, CA",
              "Book now",
            ),
            _mostPopularHotels(
              AssetImage('images/hotel-3.jpg'),
              "Paris, Italy",
              "\$200",
              "8 Ratings",
              "Far far away, behind the word mountains, far from the countries",
              "/ night",
              "San Francisco, CA",
              "Book now",
            ),
            _mostPopularHotels(
              AssetImage('images/hotel-4.jpg'),
              "Paris, Italy",
              "\$200",
              "8 Ratings",
              "Far far away, behind the word mountains, far from the countries",
              "/ night",
              "San Francisco, CA",
              "Book now",
            ),
            _mostPopularHotels(
              AssetImage('images/hotel-5.jpg'),
              "Paris, Italy",
              "\$200",
              "8 Ratings",
              "Far far away, behind the word mountains, far from the countries",
              "/ night",
              "San Francisco, CA",
              "Book now",
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 1.2,
              color: Colors.green,
              child: Column(
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.1,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      "Our satisfied ",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        decoration: TextDecoration.none,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.04,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      "customer says",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        decoration: TextDecoration.none,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.04,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in",
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.fade,
                      style: TextStyle(
                        decoration: TextDecoration.none,
                        color: Colors.white70,
                        fontSize: 20,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.1,
                  ),
                  Column(
                    children: <Widget>[
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.6,
                        child: PageView(
                          controller: controller,
                          children: <Widget>[
                            SizedBox(
                              width: double.infinity,
                              height: MediaQuery.of(context).size.height * 0.6,
                              child: _customer(
                                  "far far away behind the world mountain, far from the countries vokalia and consonantia there live the blind texts seperated they live in",
                                  "Mac Web",
                                  "marketing manager",
                                  "images/person_1.jpg"),
                            ),
                            SizedBox(
                              width: double.infinity,
                              height: MediaQuery.of(context).size.height * 0.6,
                              child: _customer(
                                  "far far away behind the world mountain, far from the countries vokalia and consonantia there live the blind texts seperated they live in",
                                  "Mac Web",
                                  "marketing manager",
                                  "images/person_2.jpg"),
                            ),
                            SizedBox(
                              width: double.infinity,
                              height: MediaQuery.of(context).size.height * 0.6,
                              child: _customer(
                                  "far far away behind the world mountain, far from the countries vokalia and consonantia there live the blind texts seperated they live in",
                                  "Mac Web",
                                  "marketing manager",
                                  "images/person_1.jpg"),
                            ),
                            SizedBox(
                              width: double.infinity,
                              height: MediaQuery.of(context).size.height * 0.6,
                              child: _customer(
                                  "far far away behind the world mountain, far from the countries vokalia and consonantia there live the blind texts seperated they live in",
                                  "Mac Web",
                                  "marketing manager",
                                  "images/person_1.jpg"),
                            ),
                            SizedBox(
                              width: double.infinity,
                              height: MediaQuery.of(context).size.height * 0.6,
                              child: Card(
                                color: Colors.greenAccent,
                                child: _customer(
                                    "far far away behind the world mountain, far from the countries vokalia and consonantia there live the blind texts seperated they live in",
                                    "Mac Web",
                                    "marketing manager",
                                    "images/person_1.jpg"),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: SmoothPageIndicator(
                          controller: controller,
                          count: 5,
                          effect: JumpingDotEffect(),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            Container(
              height: len / 2,
              decoration: BoxDecoration(color: Colors.white),
              child: Center(
                  child: Text(
                "Recommended Restaurants",
                textAlign: TextAlign.center,
                style: TextStyle(
                  decoration: TextDecoration.none,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: MediaQuery.of(context).size.height * 0.05,
                ),
              )),
            ),
            _mostPopularRestaurants(
              AssetImage('images/restaurant-1.jpg'),
              "Luxury Restaurant",
              "8 Rating",
              "Far far away, behind the word mountains, far from the countries",
              "San Franciso, CA",
              "Book now",
            ),
            _mostPopularRestaurants(
              AssetImage('images/restaurant-2.jpg'),
              "Luxury Restaurant",
              "8 Rating",
              "Far far away, behind the word mountains, far from the countries",
              "San Franciso, CA",
              "Book now",
            ),
            _mostPopularRestaurants(
              AssetImage('images/restaurant-3.jpg'),
              "Luxury Restaurant",
              "8 Rating",
              "Far far away, behind the word mountains, far from the countries",
              "San Franciso, CA",
              "Book now",
            ),
            _mostPopularRestaurants(
              AssetImage('images/restaurant-4.jpg'),
              "Luxury Restaurant",
              "8 Rating",
              "Far far away, behind the word mountains, far from the countries",
              "San Franciso, CA",
              "Book now",
            ),
            Container(
                decoration: BoxDecoration(color: Colors.blue[50]),
                child: Column(children: <Widget>[
                  Container(
                    height: len / 4,
                    decoration: BoxDecoration(color: Colors.transparent),
                    child: Center(
                        child: Text(
                      "Tips \$ Articles",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        decoration: TextDecoration.none,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: MediaQuery.of(context).size.height * 0.05,
                      ),
                    )),
                  ),
                  _imagecardsplustext(
                      AssetImage('images/image_1.jpg'),
                      "Tips, Travel",
                      "October 3, 2018 Admin",
                      "3",
                      "8 Best homestay in Philippines that you don't miss out"),
                  _imagecardsplustext(
                      AssetImage('images/image_2.jpg'),
                      "Culture",
                      "October 3, 2018 Admin",
                      "3",
                      "8 Best homestay in Philippines that you don't miss out"),
                  _imagecardsplustext(
                      AssetImage('images/image_3.jpg'),
                      "Tips, Travel",
                      "October 3, 2018 Admin",
                      "3",
                      "8 Best homestay in Philippines that you don't miss out"),
                ])),
            Center(
              child: Container(
                  height: len / 1.2,
                  decoration: BoxDecoration(color: Colors.greenAccent),
                  child: Column(children: <Widget>[
                    SizedBox(height: len / 10),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Center(
                        child: Text(
                          "Subcribe to our",
                          style: TextStyle(
                            decoration: TextDecoration.none,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: MediaQuery.of(context).size.height * 0.04,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Center(
                        child: Text(
                          "Newsletter",
                          style: TextStyle(
                            decoration: TextDecoration.none,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: MediaQuery.of(context).size.height * 0.04,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: len / 15),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in",
                        style: TextStyle(
                          decoration: TextDecoration.none,
                          color: Colors.white,
                          fontWeight: FontWeight.normal,
                          fontSize: MediaQuery.of(context).size.height * 0.03,
                        ),
                      ),
                    ),
                    SizedBox(height: len / 10),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.white)),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "Email address",
                                style: TextStyle(
                                  decoration: TextDecoration.none,
                                  color: Colors.white,
                                  fontWeight: FontWeight.normal,
                                  fontSize: MediaQuery.of(context).size.height *
                                      0.027,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.white)),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: GestureDetector(
                                onTap: () {},
                                child: Text(
                                  "Subscribe",
                                  style: TextStyle(
                                    decoration: TextDecoration.none,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize:
                                        MediaQuery.of(context).size.height *
                                            0.027,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ])
                  ])),
            )
          ],
        ),
      ),
    );
  }
}
